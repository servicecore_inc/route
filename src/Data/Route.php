<?php

namespace ServiceCore\Route\Data;

/**
 * A route
 *
 * A route is composed of route segments.
 */
class Route
{
    /**
     * @var  Segment[]  the route's segments
     */
    private $segments;

    /**
     * Called when the route is constructed
     *
     * @param  Segment[]  $segments  the route's segments
     */
    public function __construct(array $segments)
    {
        $this->segments = $segments;
    }

    /**
     * Returns the route's segments
     *
     * @return  Segment[]
     */
    public function getSegments(): array
    {
        return $this->segments;
    }

    /**
     * Returns the first segment
     *
     * @return  Segment|false
     */
    public function getFirst()
    {
        return reset($this->segments);
    }

    /**
     * Returns the last segment
     *
     * @return  Segment|false
     */
    public function getLast()
    {
        return end($this->segments);
    }
}
