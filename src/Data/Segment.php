<?php

namespace ServiceCore\Route\Data;

/**
 * A route segment
 */
class Segment
{
    /**
     * @var  mixed[]  the route segment's configuration, for example...
     *
     *         [
     *             'type' => 'segment',
     *             'options' => [
     *                 'route' => '/foo/[:foo_id]',
     *                 'defaults' => [
     *                     'controller' => 'Path\To\Foo'
     *                 ],
     *                 'constraints' => [
     *                     'foo_id' => '\d+'
     *                 ]
     *             ]
     *         ]
     */
    private $configuration;

    /**
     * @var  string  the segment's name
     */
    private $name;
    
    /**
     * Called when the segment is constructed
     *
     * @param string $name
     *
     * @param array $configuration
     */
    public function __construct(string $name, array $configuration)
    {
        $this->name          = $name;
        $this->configuration = $configuration;
    }

    /**
     * Returns the segment's configuration
     *
     * @return  mixed[]
     */
    public function getConfiguration(): array
    {
        return $this->configuration;
    }

    /**
     * Returns the segment's name
     *
     * @return  string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
