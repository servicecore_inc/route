<?php

namespace ServiceCore\Route\Context;

use ServiceCore\Route\Data\Route;
use ServiceCore\Route\Data\Segment;
use OutOfBoundsException;

/**
 * The explode-route context
 */
class Explode
{
    /**
     * @var  mixed[]  the routes configuration array in the application's service
     *     manager, for example...
     *
     *         [
     *             'foo' => [
     *                 ...
     *                 'child_routes' => [
     *                     'bar' => [
     *                         ...,
     *                         'child_routes' => [
     *                             'baz' => [
     *                                 ...
     *                             ]
     *                         ]
     *                     ]
     *                 ]
     *             ]
     *         ]
     *
     */
    private $routes;

    /**
     * Called when the context is constructed
     *
     * @param  mixed[] $routes the application's routes configuration
     */
    public function __construct(array $routes)
    {
        $this->routes = $routes;
    }

    /**
     * Explodes a route into segments
     *
     * @param   string $route the route name (e.g., "/foo/bar")
     *
     * @return  Route
     * @throws  OutOfBoundsException  if a route segment does not exist in the routes
     *     configuration array
     */
    public function explode(string $route): Route
    {
        $segments = [];

        // initialize the loop's configuration array
        $configuration = $this->routes;

        // get the route's segment names...
        // keep in mind, make sure you filter out null values, because an empty
        //    string ("") or the root route ("/") will result in an array with
        //    a single null element (e.g., [null])
        //
        $names = array_filter(explode('/', $route), 'mb_strlen');

        // if segments exist
        if ($names) {
            // loop through the segment names
            foreach ($names as $k => $name) {
                // if the current segment doesn't exist, short-circuit
                if (!array_key_exists($name, $configuration)) {
                    throw new OutOfBoundsException(
                        __METHOD__ . "() expects the route segment '{$name}' to exist "
                        . "as a child route of the current routes array, '"
                        . implode('.', array_slice($names, 0, $k + 1)) . "'"
                    );
                }

                // otherwise, get the current segment's configuration
                $configuration = $configuration[$name];

                // create and append a new exploded segment to the array
                $segments[] = new Segment($name, $this->trim($configuration));

                // if the current segment's configuration has child routes
                if (array_key_exists('child_routes', $configuration)) {
                    $configuration = $configuration['child_routes'];
                } else {
                    $configuration = [];
                }
            }
        }

        return new Route($segments);
    }

    /**
     * Trims the configuration array
     *
     * I'll remove any un-wanted configuration options like "child_routes", etc.
     *
     * @param   mixed[] $configuration the configuration to trim
     *
     * @return  mixed[]
     */
    private function trim(array $configuration): array
    {
        if (array_key_exists('child_routes', $configuration)) {
            unset($configuration['child_routes']);
        }

        return $configuration;
    }
}
