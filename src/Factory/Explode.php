<?php

namespace ServiceCore\Route\Factory;

use Interop\Container\ContainerInterface;
use ServiceCore\Route\Context\Explode as Context;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * The factory for the explode-route context
 */
class Explode implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return Context
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): Context
    {
        return new Context($container->get('config')['router']['routes']);
    }
}
