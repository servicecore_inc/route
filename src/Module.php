<?php

namespace ServiceCore\Route;

use Zend\ModuleManager\Feature\ConfigProviderInterface;

/**
 * The route module
 *
 * See README.md for details.
 */
class Module implements ConfigProviderInterface
{
    /**
     * @return mixed
     */
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
}
