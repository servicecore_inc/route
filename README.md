# Route 

The route module explodes a route into route segments (e.g., in psuedo-code, the string `"foo/bar"` becomes an array of objects `[{foo}, {bar}]`). Each route segment contains its configuration array in the service manager.

This module is necessary, because as far as Jason and I (Jack) can tell, Zend doesn't make the configuration arrays of a route's segments available.

This module is small; it doesn't provide API enpoints; and, it handles no framework events. Arguably, it's a great candidate for a separate repository. However, I (Jack) don't have time to deal with that right now.

## Example

Of course, keep in mind, defining the `$routes` configuration array and instantiating the context are _only required for this example_. In real life, you would get the context from the service manager, and it'd be configured with the application's routes configuration array.

```
// define the routes configuration array
$routes = [
    'foo' => [
        'foo' => 'bar',
        'child_routes' => [
            'bar' => [
                'bar' => 'baz',
                'child_routes' => [
                    'baz' => [
                        'baz' => 'qux'
                    ]
                ]
            ]
        ]
    ] 
];

// instantiate the context
$context = new Explode($routes);

// explode the route into segments
$segments = $context->explode('/foo/bar/baz');

// loop through the segments
foreach ($segments as $segment) {
    var_dump($segment);
}
```

The example above would output something similar to the following:

```
['foo' => 'bar']
['bar' => 'baz']
['baz' => 'qux']
```

That's about it! The "exploder" is pretty simple!
