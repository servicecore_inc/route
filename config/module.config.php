<?php

namespace ServiceCore\Route;

return [
    'service_manager' => [
        'factories' => [
            Context\Explode::class  => Factory\Explode::class
        ],
    ],
];
