<?php

namespace ServiceCore\Route\Test;

use ServiceCore\Route\Context\Explode;
use ServiceCore\Route\Data\{Route, Segment};
use OutOfBoundsException;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

class ExplodeTest extends TestCase
{
    public function testConstructor(): void
    {
        $routes            = ['foo' => 'bar'];
        $context           = new Explode($routes);
        $reflectedContext  = new ReflectionClass(Explode::class);
        $reflectedProperty = $reflectedContext->getProperty('routes');

        $reflectedProperty->setAccessible(true);

        $this->assertEquals($routes, $reflectedProperty->getValue($context));
    }

    public function testExplodeReturnsRouteIfRouteIsEmpty(): void
    {
        $context = new Explode([]);

        $this->assertEquals(new Route([]), $context->explode(''));
    }

    public function testExplodeReturnsRouteIfRouteIsRoot(): void
    {
        $context = new Explode([]);

        $this->assertEquals(new Route([]), $context->explode('/'));
    }

    public function testExplodeThrowsExceptionIfRouteIsMissing(): void
    {
        // note the key "qux" does not exist in the routes array
        $routes = ['foo' => ['bar' => 'baz']];

        $this->expectException(OutOfBoundsException::class);

        (new Explode($routes))->explode('qux');
    }

    public function testExplodeThrowsExceptionIfChildRouteIsMissing(): void
    {
        // note the key "qux" does not exist in the child_routes array
        $routes = ['foo' => ['child_routes' => ['bar' => 'baz']]];

        $this->expectException(OutOfBoundsException::class);

        (new Explode($routes))->explode('foo/qux');
    }

    public function testExplodeReturnsRouteIfRouteIsValid(): void
    {
        $foo = ['foo' => 'bar'];
        $bar = ['bar' => 'baz'];

        // nest $bar inside $foo using Zend's route configuration
        $routes = ['foo' => array_merge($foo, ['child_routes' => ['bar' => $bar]])];

        // we expect a route with two segments
        $expected = new Route(
            [
                new Segment('foo', $foo),
                new Segment('bar', $bar)
            ]
        );

        $this->assertEquals($expected, (new Explode($routes))->explode('/foo/bar'));
    }
}
