<?php

namespace ServiceCore\Route\Test;

use ServiceCore\Route\Context\Explode as Context;
use ServiceCore\Route\Factory\Explode as Factory;
use PHPUnit\Framework\TestCase;
use Zend\ServiceManager\Factory\InvokableFactory;
use Zend\ServiceManager\ServiceManager;

class ExplodeFactoryTest extends TestCase
{
    public function testCreateService(): void
    {
        $config = [
            'router' => [
                'routes' => [
                    'foo' => 'bar'
                ]
            ],
            'service_manager' => [
                'factories' => [
                    'config' => InvokableFactory::class
                ]
            ]
        ];

        $services = new ServiceManager();

        $services->setService('config', $config);

        $factory = new Factory();

        $this->assertInstanceOf(Context::class, $factory($services, ''));
    }
}
