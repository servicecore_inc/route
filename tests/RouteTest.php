<?php

namespace ServiceCore\Route\Test;

use ServiceCore\Route\Data\Route;
use ServiceCore\Route\Data\Segment;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

class RouteTest extends TestCase
{
    public function testConstructor(): void
    {
        $segments          = [new Segment('foo', ['bar' => 'baz'])];
        $route             = new Route($segments);
        $reflectedRoute    = new ReflectionClass(Route::class);
        $reflectedProperty = $reflectedRoute->getProperty('segments');

        $reflectedProperty->setAccessible(true);

        $this->assertSame($segments, $reflectedProperty->getValue($route));
    }

    public function testGetSegments(): void
    {
        $segments          = [new Segment('foo', ['bar' => 'baz'])];
        $route             = new Route([]);
        $reflectedRoute    = new ReflectionClass(Route::class);
        $reflectedProperty = $reflectedRoute->getProperty('segments');

        $reflectedProperty->setAccessible(true);
        $reflectedProperty->setValue($route, $segments);

        $this->assertSame($segments, $route->getSegments());
    }

    public function testGetFirstReturnsFalseIfSegmentsDoNotExist(): void
    {
        $route             = new Route([]);
        $reflectedRoute    = new ReflectionClass(Route::class);
        $reflectedProperty = $reflectedRoute->getProperty('segments');

        $reflectedProperty->setAccessible(true);
        $reflectedProperty->setValue($route, []);

        $this->assertFalse($route->getFirst());
    }

    public function testGetFirstReturnsSegmentIfSegmentsDoExist(): void
    {
        $foo               = new Segment('foo', ['bar' => 'baz']);
        $bar               = new Segment('qux', ['quux' => 'corge']);
        $segments          = [$foo, $bar];
        $route             = new Route([]);
        $reflectedRoute    = new ReflectionClass(Route::class);
        $reflectedProperty = $reflectedRoute->getProperty('segments');

        $reflectedProperty->setAccessible(true);
        $reflectedProperty->setValue($route, $segments);

        $this->assertSame($foo, $route->getFirst());
    }

    public function testGetLastReturnsFalseIfSegmentsDoNotExist(): void
    {
        $route             = new Route([new Segment('foo', ['bar' => 'baz'])]);
        $reflectedRoute    = new ReflectionClass(Route::class);
        $reflectedProperty = $reflectedRoute->getProperty('segments');

        $reflectedProperty->setAccessible(true);
        $reflectedProperty->setValue($route, []);

        $this->assertFalse($route->getLast());
    }

    public function testGetLastReturnsSegmentIfSegmentsDoExist(): void
    {
        $foo               = new Segment('foo', ['bar' => 'baz']);
        $bar               = new Segment('qux', ['quux' => 'corge']);
        $segments          = [$foo, $bar];
        $route             = new Route([]);
        $reflectedRoute    = new ReflectionClass(Route::class);
        $reflectedProperty = $reflectedRoute->getProperty('segments');

        $reflectedProperty->setAccessible(true);
        $reflectedProperty->setValue($route, $segments);

        $this->assertSame($bar, $route->getLast());
    }
}
