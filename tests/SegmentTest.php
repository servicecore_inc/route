<?php

namespace ServiceCore\Route\Test;

use ServiceCore\Route\Data\Segment;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

class SegmentTest extends TestCase
{
    public function testConstructor(): void
    {
        $name             = 'foo';
        $config           = ['bar' => 'baz'];
        $segment          = new Segment($name, $config);
        $reflectedSegment = new ReflectionClass(Segment::class);
        $reflectedName    = $reflectedSegment->getProperty('name');
        $reflectedConfig  = $reflectedSegment->getProperty('configuration');

        $reflectedName->setAccessible(true);
        $reflectedConfig->setAccessible(true);

        $this->assertEquals($name, $reflectedName->getValue($segment));
        $this->assertEquals($config, $reflectedConfig->getValue($segment));
    }

    public function testGetName(): void
    {
        $name              = 'foo';
        $segment           = new Segment('bar', []);
        $reflectedSegment  = new ReflectionClass(Segment::class);
        $reflectedProperty = $reflectedSegment->getProperty('name');

        $reflectedProperty->setAccessible(true);
        $reflectedProperty->setValue($segment, $name);

        $this->assertEquals($name, $segment->getName());
    }

    public function testGetConfiguration(): void
    {
        $config            = ['foo' => 'bar'];
        $segment           = new Segment('foo', []);
        $reflectedSegment  = new ReflectionClass(Segment::class);
        $reflectedProperty = $reflectedSegment->getProperty('configuration');

        $reflectedProperty->setAccessible(true);
        $reflectedProperty->setValue($segment, $config);

        $this->assertEquals($config, $segment->getConfiguration());
    }
}
